# Longevity InTime petitions
Translated a petition? edit the json and make a merge request!

## How to add a link
_You can skip this if is used to work with git_

1. log in your gitlab account (_or clone the repository, and skip to item 7_)

2. Click on Web IDE or click on petitions.json and edit/Web IDE button
    * ![image.png](./image.png)
    * ![image-1.png](./image-1.png)

3. Add the link
    * Search for the country ("Find in page", F3 or ctrl+F)
        - Let's say you want to add ```Denmark```
        - Search for it ![image-2.png](./image-2.png)
        - Browser will highlight next occurence of the word "Denmark", check if it is correct
        - Add the link on the "petitionUrl":"here" ![image-3.png](./image-3.png)
            - example: "petitionUrl": "https://change.org/DenmarkPetition"
    * Then, go back to top, in "petition-open" and add "<\continent>.<\country-code>"
        - example: Europe.UK
        - example: America.US

4. After finish editing, click on the button "Commit" at the left side
    * type a message saying what you have done
    * mark the box "Create new branch"
    * mark the box "Start a new merge request"

5. Click on commit to finish

6. wait for it to be reviewed and/or merged to main branch

7. Using your own text/file editor?
    * open console (terminal/bash/cmd/powershell...)
    * clone the repository ```git clione https://gitlab.com/mmatheus/intime-petitions.git```
    * enter the folder in terminal (if already didn't with ```cd intime-petitions```)
    * create a branch ```git checkout -b myNewBranch```
    * open the file petitions.json and edit to add the link to the country
    * add a line in "petitions-open" containing the <\continent>.<\country> (see other lines as example)
    * add all files to commit: ```git add .``` or only the petitions.json: ```git add ./petitions.json```
    * make a commit ```git commit -m "added a link for country ... "```
    * come back here on gitlab and make a merge request

## Credits

Json country list based on [samayo/country-json repository at Github](https://github.com/samayo/country-json)
